import java.util.*;

public class sort {
	static Scanner inp = new Scanner(System.in);

	public static void main(String args[]) {
//		variable class Student
		int id;
		double gpa;
		String name;

		int n = inp.nextInt();
		char ad = inp.next().charAt(0);
		String type = inp.next();
		List<student> stud = new ArrayList<student>();
		for (int i = 0; i < n; i++) {
			id = inp.nextInt();
			name = inp.next();
			gpa = inp.nextDouble();
			student stu = new student(id, name, gpa);
			stud.add(stu);
		}
		if (type.equals("id")) {
			if (ad == 'a') {
				Collections.sort(stud, student.studentIDcomp);
			} else if (ad == 'd') {
				Collections.sort(stud, student.studentIDcomp.reversed());
			}
		}
		if (type.equals("name")) {
			if (ad == 'a') {
				Collections.sort(stud, student.studentNamecomp);
			} else if (ad == 'd') {
				Collections.sort(stud, student.studentNamecomp.reversed());
			}
		}
		if (type.equals("gpa")) {
			if (ad == 'a') {
				Collections.sort(stud, student.studentGpacomp);
			} else if (ad == 'd') {
				Collections.sort(stud, student.studentGpacomp.reversed());
			}
		}

		for (student i : stud) {
			System.out.println(i.getid() + " " + i.getname() + " " + i.getgpa());
		}
	}
}

class student implements Comparable<student> {
	private int id;
	private double gpa;
	private String name;

	public student(int id, String name, double gpa) {
		this.id = id;
		this.name = name;
		this.gpa = gpa;
	}

//	set
	public void setid(int id) {
		this.id = id;
	}

	public void setgpa(double gpa) {
		this.gpa = gpa;
	}

	public void setname(String name) {
		this.name = name;
	}

//	get
	public int getid() {
		return this.id;
	}

	public double getgpa() {
		return this.gpa;
	}

	public String getname() {
		return this.name;
	}

	public static Comparator<student> studentIDcomp = new Comparator<student>() {

		@Override
		public int compare(student name1, student name2) {
			int studentid1 = name1.getid();
			int studentid2 = name2.getid();
			return studentid1 - studentid2;
		}
	};

	public static Comparator<student> studentNamecomp = new Comparator<student>() {

		@Override
		public int compare(student name1, student name2) {
			String studentname1 = name1.getname().toUpperCase();
			String studentname2 = name2.getname().toUpperCase();
			return studentname1.compareTo(studentname2);
		}
	};

	public static Comparator<student> studentGpacomp = new Comparator<student>() {
		@Override
		public int compare(student name1, student name2) {
			double studentgpa1 = name1.getgpa();
			double studentgpa2 = name2.getgpa();
			if (studentgpa1 > studentgpa2) {
				return 1;
			}
			if (studentgpa1 < studentgpa2) {
				return -1;
			}
			return 0;
		}
		/* can use alternative return
		 * return Double.compare(studentgpa1, studentgpa2);
		 */
	};

	@Override
	public int compareTo(student o) {
		// TODO Auto-generated method stub
		return 0;
	}
}
